// player types

export type PlayerType = {
  id: number;
  name: string;
  score: number;
};

export type PlayersList = PlayerType[];

// game types

export type CardType = {
  id: number;
  text: string;
  active: boolean;
  matched: boolean;
};

export type CardsList = CardType[];

export type PairType = {
  question: number;
  answer: number;
  matched: boolean;
};

export type PairsList = PairType[];
