import React, { useState } from "react";
import { CardsList, PlayersList } from "../../types";
import { GAME_CARDS, MATCHING_DUOS } from "../../constants";

import UserTurns from "./components/UserTurns/UserTurns";

import "./GameScreenStyles.css";
import GameBoard from "./components/GameBoard/GameBoard";
import produce from "immer";
import RightAnswers from "./components/RightAnswers/RightAnswers";

export type GameScreenType = {
  players: PlayersList;
  updatePlayers: (newPlayers: PlayersList) => void;
};

const shuffleGameCards = () => {
  const cards = [...GAME_CARDS];
  return cards.sort(() => Math.random() - 0.5);
};

const GameScreen = ({ players, updatePlayers }: GameScreenType) => {
  const [activeUserIndex, updateActiveUserIndex] = useState(0);
  const [gameCards, updateGameCards] = useState<CardsList>(shuffleGameCards());
  const [cardDuos, updateCardDuos] = useState(MATCHING_DUOS);

  const handleNextPlayerClick = () =>
    updateActiveUserIndex(
      activeUserIndex === players.length - 1 ? 0 : activeUserIndex + 1
    );

  const handleAddPointsToPlayer = () => {
    const newPlayers = produce(players, (draft: PlayersList) => {
      draft[activeUserIndex].score += 1;
    });
    updatePlayers(newPlayers);
  };

  return (
    <div className="game-screen-container">
      <UserTurns
        players={players}
        activePlayerIndex={activeUserIndex}
        setNextPlayer={updateActiveUserIndex}
      />
      <GameBoard
        gameCards={gameCards}
        updateGameCards={updateGameCards}
        cardDuos={cardDuos}
        updateCardDuos={updateCardDuos}
        nextPlayer={handleNextPlayerClick}
        addPointsToPlayer={handleAddPointsToPlayer}
      />
      <RightAnswers cardDuos={cardDuos} cardList={gameCards} />
    </div>
  );
};

export default GameScreen;
