import React, { useEffect, useRef } from "react";
import styled from "@emotion/styled";
import { ArrowForwardIos } from "@material-ui/icons";

const ActiveIcon = styled(ArrowForwardIos)<{ visible: boolean }>`
  font-size: 2rem !important;
  color: #00ced1;
  visibility: ${({ visible }) => (visible ? "visible" : "hidden")};
`;

const SingleUserContainer = styled.div<{ active: boolean }>`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  text-transform: uppercase;
  font-size: 1.2rem;
  color: ${({ active }) => (active ? "#00ced1" : "#feda4a")};
  font-weight: ${({ active }) => active && "bold"};
`;

export type SingleUserType = {
  name: string;
  score: number;
  active: boolean;
};

const SingleUser = ({ name, active, score }: SingleUserType) => {
  const ref = useRef<HTMLDivElement>(null);

  const scrollOnItem = () => {
    if (ref.current) {
      ref.current.scrollIntoView({
        behavior: "smooth",
      });
    }
  };

  useEffect(() => {
    if (active) {
      scrollOnItem();
    }
  }, [active]);

  return (
    <SingleUserContainer active={active}>
      <ActiveIcon visible={active} />
      <span>{name}</span>
      <span className="score">{score}</span>
    </SingleUserContainer>
  );
};

export default SingleUser;
