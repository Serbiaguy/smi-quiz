import React, { useEffect } from "react";
import { PlayersList, PlayerType } from "../../../../types";

import "./styles.css";
import SingleUser from "./SingleUser";

const ARROW_UP = "ArrowUp";
const ARROW_DOWN = "ArrowDown";

export type UserTurnsType = {
  players: PlayersList;
  activePlayerIndex: number;
  setNextPlayer: (newPlayerIndex: number) => void;
};

const UserTurns = ({
  players,
  activePlayerIndex,
  setNextPlayer,
}: UserTurnsType) => {
  const showPlayers = (user: PlayerType, index: number) => {
    const { name, score } = user;
    return (
      <SingleUser
        name={name}
        score={score}
        active={index === activePlayerIndex}
      />
    );
  };

  const keyPressHandler = ({ key }: KeyboardEvent) => {
    const playersCount = players.length;
    if (key === ARROW_UP) {
      setNextPlayer(
        activePlayerIndex === 0 ? playersCount - 1 : activePlayerIndex - 1
      );
    }
    if (key === ARROW_DOWN) {
      setNextPlayer(
        activePlayerIndex === playersCount - 1 ? 0 : activePlayerIndex + 1
      );
    }
  };

  useEffect(() => {
    window.addEventListener("keydown", keyPressHandler, false);
    return () => {
      window.removeEventListener("keydown", keyPressHandler, false);
    };
  }, [activePlayerIndex]);

  return (
    <div className="user-turns-container">
      <div className="user-turns-list">
        <div className="user-turns-header">
          <span />
          <span>jméno</span>
          <span>skóre</span>
        </div>
        <div className="overflow-list-wrapper">{players.map(showPlayers)}</div>
      </div>
    </div>
  );
};

export default UserTurns;
