import React, { useEffect, useState } from "react";
import { produce } from "immer";
import { CardsList, CardType, PairsList } from "../../../../types";

import "./styles.css";
import GameCard from "./GameCard";
import useFireworks from "./useFireworks";

export type GameBoardType = {
  gameCards: CardsList;
  updateGameCards: (newGameCards: CardsList) => void;
  cardDuos: PairsList;
  updateCardDuos: (newCardDuos: PairsList) => void;
  nextPlayer: () => void;
  addPointsToPlayer: () => void;
};

const GameBoard = ({
  gameCards,
  updateCardDuos,
  cardDuos,
  updateGameCards,
  nextPlayer,
  addPointsToPlayer,
}: GameBoardType) => {
  const [firstClicked, setFirstClicked] = useState<number | undefined>(
    undefined
  );
  const [secondClicked, setSecondClicked] = useState<number | undefined>(
    undefined
  );

  const fireworks = useFireworks();

  const getCardDuoByFirstClicked = () => {
    return cardDuos.filter(
      (item) => item.answer === firstClicked || item.question === firstClicked
    )[0];
  };

  const getIndexOfSecondClicked = () => {
    let i = -1;
    gameCards.forEach(({ id }, index) => {
      if (id === secondClicked) {
        i = index;
      }
    });
    return i;
  };

  const getIndexOfFirstClicked = () => {
    let i = -1;
    gameCards.forEach(({ id }, index) => {
      if (id === firstClicked) {
        i = index;
      }
    });
    return i;
  };

  const updateCardDuosFnc = (cardId: number) => {
    const newCardDuos = produce(cardDuos, (draft: PairsList) => {
      draft.forEach((item, index) => {
        if (item.answer === cardId || item.question === cardId) {
          draft[index].matched = true;
        }
      });
    });
    updateCardDuos(newCardDuos);
  };

  const everythingRevealed = () => {
    return cardDuos.filter((card) => !card.matched).length === 0;
  };

  const handleCardClick = (cardId: number, index: number) => {
    if (firstClicked === undefined) {
      setFirstClicked(cardId);
      const updatedCards = produce(gameCards, (draft: CardsList) => {
        draft[index].active = true;
      });
      updateGameCards(updatedCards);
      return;
    }

    if (secondClicked === undefined) {
      if (firstClicked !== cardId) {
        setSecondClicked(cardId);
        const cardDuo = getCardDuoByFirstClicked();
        const firstIndex = getIndexOfFirstClicked();

        if (cardDuo.answer === cardId || cardDuo.question === cardId) {
          const updatedCards = produce(gameCards, (draft: CardsList) => {
            draft[firstIndex].matched = true;
            draft[index].matched = true;
            draft[index].active = true;
          });
          updateGameCards(updatedCards);
          updateCardDuosFnc(cardId);
          addPointsToPlayer();
        } else {
          const updatedCards = produce(gameCards, (draft: CardsList) => {
            draft[index].active = true;
          });
          updateGameCards(updatedCards);
        }
      }
      return;
    }

    const firstClickedIndex = getIndexOfFirstClicked();
    const secondClickedIndex = getIndexOfSecondClicked();
    const updatedCards = produce(gameCards, (draft: CardsList) => {
      draft[firstClickedIndex].active = false;
      draft[secondClickedIndex].active = false;
      draft[index].active = true;
    });
    updateGameCards(updatedCards);
    setFirstClicked(cardId);
    setSecondClicked(undefined);
    nextPlayer();
  };

  const setLastTwoCardsToInActive = () => {
    const newCards = produce(gameCards, (draft: CardsList) => {
      const firstIndex = getIndexOfFirstClicked();
      const secondIndex = getIndexOfSecondClicked();
      if (firstIndex !== -1 && secondIndex !== -1) {
        draft[firstIndex].active = false;
        draft[secondIndex].active = false;
      }
    });
    updateGameCards(newCards);
  };

  useEffect(() => {
    console.log("render");
    if (everythingRevealed()) {
      setTimeout(() => {
        setLastTwoCardsToInActive();
        fireworks();
      }, 2000);
    }
  }, [cardDuos]);

  const showGameCards = (card: CardType, index: number) => {
    const { text, id, active, matched } = card;
    return (
      <GameCard
        text={text}
        active={active}
        matched={matched}
        onCardClick={() => handleCardClick(id, index)}
      />
    );
  };

  return (
    <div className="game-board-container">{gameCards.map(showGameCards)}</div>
  );
};

export default GameBoard;
