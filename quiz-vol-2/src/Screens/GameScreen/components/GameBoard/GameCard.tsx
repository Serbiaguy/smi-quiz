import React from "react";
import styled from "@emotion/styled";

export type GameCardType = {
  text: string;
  active: boolean;
  matched: boolean;
  onCardClick: () => void;
};

const GameCardContainer = styled.div<{ active: boolean; matched: boolean }>`
  width: 7rem;
  height: 7rem;
  border-radius: 10px;
  margin: 0.5rem;
  padding: 1rem;
  text-align: center;
  color: black;
  font-size: 1rem;
  border: 2px solid transparent;
  font-weight: bold;
  display: flex;
  align-items: center;
  justify-content: center;

  ${({ active, matched }) => {
    if (matched && active) {
      return `
      background-color: #4bb543;
      box-shadow: 0 0 15px #4bb543;
      `;
    }
    if (matched) {
      return `
        border: 2px solid #feda4a;
        box-shadow: 0 0 5px #feda4a;
        background-color: transparent;
        color: #feda4a;
      `;
    }
    if (active) {
      return `
        background-color: #00ced1;
        cursor: pointer;
        box-shadow: 0 0 15px #00ced1;
      `;
    }
    return `
        background-color: #feda4a;
        cursor: pointer;
        font-size: 2rem;
        font-weight: bold;
        
        &:hover {
          box-shadow: 0 0 15px #feda4a;
        }
      `;
  }}

  span {
    margin: auto 0;
  }
`;

const GameCard = ({ text, active, matched, onCardClick }: GameCardType) => {
  const wrapOnCardClick = () => {
    if (!matched) {
      onCardClick();
    }
  };
  return (
    <GameCardContainer
      active={active}
      matched={matched}
      onClick={wrapOnCardClick}
    >
      <span>{active || matched ? text : "MI-SMI"}</span>
    </GameCardContainer>
  );
};

export default GameCard;
