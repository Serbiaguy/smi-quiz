import fireworks from "fireworks";

export default () => {
  const range = [...new Array(5)];
  const halfOfWidth = window.innerWidth / 2;
  const halfOfHeight = window.innerHeight / 2;
  return () => {
    range.forEach(() => {
      setTimeout(
        () =>
          fireworks({
            x: halfOfWidth,
            y: halfOfHeight,
            colors: ["#cc3333", "#4CAF50", "#81C784"],
          }),
        500
      );
    });
  };
};
