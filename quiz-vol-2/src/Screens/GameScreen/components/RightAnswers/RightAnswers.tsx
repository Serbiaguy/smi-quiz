import React from "react";
import { CardsList, PairsList, PairType } from "../../../../types";

import "./styles.css";

export type RightAnswersType = {
  cardDuos: PairsList;
  cardList: CardsList;
};

const RightAnswers = ({ cardDuos, cardList }: RightAnswersType) => {
  const getTextForId = (id: number) => {
    let text = "";
    cardList.forEach((card) => {
      if (card.id == id) {
        text = card.text;
      }
    });
    return text;
  };

  const showAllAnswers = ({ question, answer }: PairType) => {
    const qText = getTextForId(question);
    const aText = getTextForId(answer);

    return (
      <div className="right-answer-container">
        <span className="string-container">{qText}</span>
        <span>-</span>
        <span className="string-container">{aText}</span>
      </div>
    );
  };

  return (
    <div className="right-answers-container">
      <div className="right-answers-header">
        <span>Správné odpovědi</span>
      </div>
      <div className="right-answers-wrapper">
        {cardDuos.filter((item) => item.matched).map(showAllAnswers)}
      </div>
    </div>
  );
};

export default RightAnswers;
