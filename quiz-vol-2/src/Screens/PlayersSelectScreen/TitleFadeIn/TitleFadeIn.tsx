import React, { Fragment, useEffect, useState } from "react";
import "./TitleFadeIn.css";

const TitleFadeIn = () => {
  const [showMessage, setShowMessage] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setShowMessage(true);
    }, 5000);
  });
  return (
    <div className="title-fade-container">
      {showMessage ? (
        <Fragment>
          <div className="fade"></div>
          <section className="star-wars">
            <div className="crawl">
              <div className="title">
                <p>kvíz</p>
                <h1>Winterlingova krizová matice</h1>
              </div>
              <p>Luka Lukašević</p>
              <p>Filip Nezbeda</p>
              <p>Jakub Prchal</p>
            </div>
          </section>
        </Fragment>
      ) : (
        <p className="before-title">
          Na cvičení SMI, právě teď, <br /> přes MS Teams....
        </p>
      )}
    </div>
  );
};

export default TitleFadeIn;
