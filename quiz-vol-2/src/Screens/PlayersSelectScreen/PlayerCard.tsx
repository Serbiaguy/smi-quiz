import React from "react";
import styled from "@emotion/styled";
import { HighlightOff } from "@material-ui/icons";

export type PlayerCardType = {
  name: string;
  removeUser: () => void;
};

const RemoveIcon = styled(HighlightOff)`
  color: #feda4a;
  font-size: 2rem !important;
  cursor: pointer;
  pointer-events: auto;

  &:hover {
    color: #00ced1;
  }
`;

const PlayerCard = ({ name, removeUser }: PlayerCardType) => {
  return (
    <div className="player-card-container">
      <span>{name}</span>
      <RemoveIcon onClick={removeUser} />
    </div>
  );
};

export default PlayerCard;
