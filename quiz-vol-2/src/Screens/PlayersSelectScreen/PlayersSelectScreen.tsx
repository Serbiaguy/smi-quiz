import React from "react";
import { PlayersList, PlayerType } from "../../types";

import "./styles.css";
import PlayerCard from "./PlayerCard";
import TitleFadeIn from "./TitleFadeIn/TitleFadeIn";

export type PlayersSelectScreenType = {
  players: PlayersList;
  updatePlayers: (newPlayers: PlayersList) => void;
  toggleScreen: () => void;
};

const PlayersSelectScreen = ({
  players,
  updatePlayers,
  toggleScreen,
}: PlayersSelectScreenType) => {
  const shufflePlayersArray = () => {
    const newPlayers = [...players];
    newPlayers.sort(() => Math.random() - 0.5);
    updatePlayers(newPlayers);
  };

  const showPlayers = ({ id, name }: PlayerType) => {
    return (
      <PlayerCard
        name={name}
        removeUser={() => updatePlayers(players.filter((p) => p.id !== id))}
      />
    );
  };

  const wrapStartGame = () => {
    shufflePlayersArray();
    toggleScreen();
  };

  return (
    <div className="page-container">
      <TitleFadeIn />
      <div className="players-select-container">
        <div className="players-list-selector">{players.map(showPlayers)}</div>
        <button className="add-players-button" onClick={wrapStartGame}>
          začít kvíz
        </button>
      </div>
    </div>
  );
};

export default PlayersSelectScreen;
