import { CardsList, PairsList, PlayersList } from "./types";

export const SMI_PUPILS: PlayersList = [
  {
    id: 1,
    score: 0,
    name: "Rostislav Babáček",
  },
  {
    score: 0,
    id: 2,
    name: "Adam Benda",
  },
  {
    score: 0,
    id: 3,
    name: "Miroslav Brzobohatý",
  },
  {
    score: 0,
    id: 5,
    name: "Antonín Dvořák",
  },
  {
    score: 0,
    id: 6,
    name: "Jan Fara",
  },
  {
    score: 0,
    id: 8,
    name: "Jaroslav Hlaváč",
  },
  {
    score: 0,
    id: 11,
    name: "Jan Jindráček",
  },
  {
    score: 0,
    id: 15,
    name: "Vojtěch Kolář",
  },
  {
    score: 0,
    id: 16,
    name: "Martin Kutiš",
  },
  {
    score: 0,
    id: 17,
    name: "Tomáš Lála",
  },
  {
    score: 0,
    id: 18,
    name: "Jan Přívratský",
  },
  {
    score: 0,
    id: 21,
    name: "Radek Smejkal",
  },
  {
    score: 0,
    id: 22,
    name: "Michal Šveigr",
  },
  {
    score: 0,
    id: 23,
    name: "Štěpán Tužil",
  },
  {
    score: 0,
    id: 26,
    name: "Petra Pavlíčková",
  },
];

export const GAME_CARDS: CardsList = [
  {
    id: 1,
    active: false,
    text: "Jak se nazývá matice pro analýzu rizik?",
    matched: false,
  },
  {
    id: 2,
    active: false,
    text: "Strategie rozhodování 4T",
    matched: false,
  },
  {
    id: 3,
    active: false,
    text: "V co může riziko přerůst?",
    matched: false,
  },
  {
    id: 4,
    active: false,
    text: "Druhy rizik",
    matched: false,
  },
  {
    id: 5,
    active: false,
    text: "Druhy metod, jak se rizika vyhodnocují",
    matched: false,
  },
  {
    id: 6,
    active: false,
    text: "Jak se dá jinak nazvat kvalitativní metoda?",
    matched: false,
  },
  {
    id: 7,
    active: false,
    text: "Jak se dá jinak nazvat kvantitavní metoda?",
    matched: false,
  },
  {
    id: 8,
    active: false,
    text: "Co je matice rizik?",
    matched: false,
  },
  {
    id: 9,
    active: false,
    text: 'Jak by jste definovali strategii rozhodování "Transfer"?',
    matched: false,
  },
  {
    id: 10,
    active: false,
    text: "Líbila se Vám prezentace?",
    matched: false,
  },
  {
    id: 11,
    active: false,
    text: "Winterlingova matice",
    matched: false,
  },
  {
    id: 12,
    active: false,
    text: "Take, treat, transfer, terminate",
    matched: false,
  },
  {
    id: 13,
    active: false,
    text: "V krizi",
    matched: false,
  },
  {
    id: 14,
    active: false,
    text: "Vnitřní, vnější, politická, legislativní a další",
    matched: false,
  },
  {
    id: 15,
    active: false,
    text: "Kvalitativní a kvantitativní",
    matched: false,
  },
  {
    id: 16,
    active: false,
    text: "Subjektivní",
    matched: false,
  },
  {
    id: 17,
    active: false,
    text: "Pravděpodobnostní",
    matched: false,
  },
  {
    id: 18,
    active: false,
    text: "Seznam rizik spolu s ohodnocením, druhem, dopadem, mitigací atd.",
    matched: false,
  },
  {
    id: 19,
    active: false,
    text: "Úplatné přenesení rizika na třetí osobu, sdílení rizika.",
    matched: false,
  },
  {
    id: 20,
    active: false,
    text: "ANO!!!",
    matched: false,
  },
];

export const MATCHING_DUOS: PairsList = [
  {
    question: 1,
    answer: 11,
    matched: false,
  },
  {
    question: 2,
    answer: 12,
    matched: false,
  },
  {
    question: 3,
    answer: 13,
    matched: false,
  },
  {
    question: 4,
    answer: 14,
    matched: false,
  },
  {
    question: 5,
    answer: 15,
    matched: false,
  },
  {
    question: 6,
    answer: 16,
    matched: false,
  },
  {
    question: 7,
    answer: 17,
    matched: false,
  },
  {
    question: 8,
    answer: 18,
    matched: false,
  },
  {
    question: 9,
    answer: 19,
    matched: false,
  },
  {
    question: 10,
    answer: 20,
    matched: false,
  },
];
