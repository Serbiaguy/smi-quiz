import React, { useState } from "react";
import "./App.css";
import GameScreen from "./Screens/GameScreen";
import PlayersSelectScreen from "./Screens/PlayersSelectScreen";
import { SMI_PUPILS } from "./constants";

const App = () => {
  const [gameStarted, toggleGameStarted] = useState(false);
  const [playersList, updatePlayersList] = useState(SMI_PUPILS);
  return (
    <div className="App">
      {gameStarted ? (
        <GameScreen players={playersList} updatePlayers={updatePlayersList} />
      ) : (
        <PlayersSelectScreen
          players={playersList}
          toggleScreen={() => toggleGameStarted(true)}
          updatePlayers={updatePlayersList}
        />
      )}
    </div>
  );
};

export default App;
